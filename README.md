# Multiplayer expectations

## Описание
Интерактивный location-based VR опыт рассчитанный на 4 игроков.

* топология: клиент-сервер (ожидается вроде как выделенный сервер)
* сеть: локальная
* транспорт: TCP
* стек: C# (Mirror(UNET))

Отрытые вопросы:

* выбор транспорта (TCP vs UDP)?

## Игрок

### Описание

Игрок представляет из себя... 

Взаимодействие игрока с виртуальным миром происходит через руки. 
Большинство игровых событий будет выстроено вокруг жестов и относительной близости рук (к игровым объектам либо рукам других игроков). 
Так, например, определенный жест будет использоваться для активации светового луча - "оружие" игроков для борьбы с охотниками.

С точки зрения формы игрока определяет следующий набор объектов:  

1. голова (SteamVR)
1. руки (LeapMotion)
1. тело

С точки зрения геймплея - прежде всего руки.

### Синхронизируемые устройства
[SteamVR](https://answers.unity.com/questions/1171879/where-is-the-steamvr-vive-api-documentation.html)  
[Leap Motion Unity Modules (Core, Interaction Engine)](https://leapmotion.github.io/UnityModules/core.html)

#### VR шлем (HTC Vive Pro)
Синхронизация Transform
#### Leap Motion
Требуется реальзовать Leap Service Network Provider, умеющий передавать/принимать данные по сети. 

* атомарной сетевой единицей является [Frame](https://leapmotion.github.io/UnityModules/class_leap_1_1_frame.html).  
* базовый класс [Leap Service Provider](https://leapmotion.github.io/UnityModules/class_leap_1_1_unity_1_1_leap_service_provider.html) предоставляет метод ``DispatchUpdateFrameEvent(Frame frame)`` через который можно прокидывать  переданный по сети Frame.  
* для упаковки/распаковки Hand можно использовать специальный класс [VectorHand](https://github.com/leapmotion/UnityModules/blob/master/Assets/LeapMotion/Core/Scripts/Encoding/VectorHand.cs) 

Для тестирования может оказаться полезным пример [StationaryTestLeapProvider](https://github.com/leapmotion/UnityModules/blob/master/Assets/LeapMotion/Core/Scripts/Testing/StationaryTestLeapProvider.cs).

Открытые вопросы:

* интерполяция передаваемых данных
* тестирование на реальных устройствах (> 2)
* простая инверсная кинематика плеча/локтя на основе leap данных
* нужна ли полная отрисовка на сервере

### Текущая реализация

Игрок состоит из двух представлений: локальное и сетевое. Локальное представляет из себя префаб ``SteamVR->Player`` с добавленным к нему ``LeapServiceProvder``. По сути это контейнер получаемых с устройств данных. 
Сетевое представление (NetworkAvatar) отвечает за траспортировку по сети нужных данных и их обработку. Обработка влючает в себя как взаимодействие с игровым миром, так и отрисовку визуального представления игрока. 

1. Для синхронизации SteamVR устройств используется компонент ``NetworkTransform/NetworkTransformChild``:
2. Передача leap данных был реализован отдельный ``LeapProvider``, задачей которого является диспатчинг переданных по сети данных, которые, в свою очередь, передаются с использованием ``LeapFrameNetworkHander``
	Атомарной единицей является ``Frame``.

## Игровой мир
### Элементы игрового мира

1. Игроки
1. Интерактивные объекты
1. Флора и фауна
1. Охотники (враги)

Игровой дизайн не предполагает явного физического взаимодействия (через руки) с игровыми объектами, как следствие, нет никакой необходимости в использовании [Interaction Engine](https://leapmotion.github.io/UnityModules/interaction-engine.html).
Просчёт базовой физики для светового луча (raycast) может осуществляются как на стороне клиента, так и на стороне сервера.

Флора и фауна будет по большей части статична, не считая динамических объектов, анимация которых может быть синхронизирована по сети (``NetworkAnimator``).

В виду серверной авторитарной модели Mirror, выглядит логичным перенести все интерактивные расчёты на сторону сервера с последующей синхронизацией на стороне клиента.

Важной задачей является гарантия отказоуствойчивости и корректной синхронизации состояния игрового мира в случае переподключения одного из клиентов. 

### Синхронизируемые устройства
#### Vive Tracker
Синхронизация Transform


### Игровые события
Часть игровых событий будет построена вокруг распознавания жестов (Core/Detection Utilites) и расположения игроков и их рук относительно друг друга. Пока выглядит как базовая событийная модель c локальными/глобальными событиями. 

### Переход между сценами

